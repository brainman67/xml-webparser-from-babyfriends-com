﻿namespace BabbyFriends.Enums
{
	public enum Attributes
	{
		offer,
		id,
		available,
		url,
		price,
		currencyId,
		categoryId,
		picture,
		name,
		vendor,
		description,
		parentCategoryId,
		param,
		stockQuantity,
		category
	}
}