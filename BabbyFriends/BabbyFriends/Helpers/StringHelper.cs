﻿using System.Linq;

namespace BabbyFriends
{
	public class StringHelper
	{
		public string GetIdFromHref(string href) => string.Join("", href.Where(c => char.IsDigit(c)));
	}
}