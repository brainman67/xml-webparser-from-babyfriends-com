﻿using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Chrome;
using xNet;
using OpenQA.Selenium.Support.UI;

namespace BabbyFriends
{
	public class BrawserHelper
	{
		private const string SiteUrl = "https://baby-friends.com";
		protected IWebDriver Driver;
		public readonly HttpRequest Request;

		public BrawserHelper()
		{
			Request = new HttpRequest(SiteUrl)
			{
				UserAgent = Http.ChromeUserAgent(),
				ConnectTimeout = 120000,
				KeepAliveTimeout = 120000,
				ReadWriteTimeout = 120000,
				Authorization = "Basic dXNlcl9iYXNpYzpob21zdGVyczIwMTgh"

			};
			Request.AddHeader("Accept",
				"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
			Request.AddHeader("Upgrade-Insecure-Requests", "1");
			Request.AddHeader("Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
		}

		protected void OpenUrl(string url)
		{
			Driver = new ChromeDriver();
			var jse = (IJavaScriptExecutor)Driver;
			jse.ExecuteScript("console.clear()");

			try
			{
				Driver.Navigate().GoToUrl(url);
			}
			catch (Exception)
			{
				Console.WriteLine("Failed to open URL: " + url);
				throw new Exception();
			}
		}

		protected void RefreshCurrentPage()
		{
			Driver.Navigate().Refresh();
		}

		protected void OpenEndpoint(string endPoint)
		{
			var jse = (IJavaScriptExecutor)Driver;
			jse.ExecuteScript("console.clear()");
			Console.WriteLine("Open URL: " + SiteUrl + endPoint);
			Driver.Navigate().GoToUrl(SiteUrl + endPoint);
		}

		protected void WaitForPageLoad(TimeSpan timeout)
		{
			var waitForDocumentReady = new WebDriverWait(Driver, timeout);
			waitForDocumentReady.Until(wdriver =>
				((IJavaScriptExecutor)Driver).ExecuteScript("return document.readyState").Equals("complete"));
		}

		protected void WaitForPageLoad()
		{
			var waitForDocumentReady = new WebDriverWait(Driver, TimeSpan.FromSeconds(3));
			waitForDocumentReady.Until(wdriver =>
				((IJavaScriptExecutor)Driver).ExecuteScript("return document.readyState").Equals("complete"));
		}

		protected string GetPageSource()
		{
			Console.WriteLine("Getting page source");
			return Driver.PageSource;
		}

		public string GetPage(string endpoint)
		{
			Thread.Sleep(TimeSpan.FromSeconds(2));
			var response = endpoint.Contains(SiteUrl) ? Request.Get(endpoint) : Request.Get(SiteUrl + endpoint);
			return response.ToString();
		}

		public string GetMainPage()
		{
			//OpenUrl(SiteUrl);
			var response = Request.Get(SiteUrl);
			return response.ToString();
		}
	}
}