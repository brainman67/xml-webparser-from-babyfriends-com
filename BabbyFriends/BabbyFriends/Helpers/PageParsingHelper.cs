﻿using System;
using System.Linq;
using HtmlAgilityPack;

namespace BabbyFriends
{
	public class PageParsingHelper
	{
		// return all nodes by xpath
		public HtmlNodeCollection GetAllNodesByXpath(HtmlDocument document, string xpath)
		{
			return document.DocumentNode?.SelectNodes(xpath);
		}

		// return node by xpath
		public HtmlNode GetNodeByXpath(HtmlDocument document, string xpath)
		{
			return document.DocumentNode.SelectSingleNode(xpath);
		}

		public int GetCountOfNodes(HtmlDocument document, string xpath)
		{
			var nodes = GetAllNodesByXpath(document, xpath);
			return nodes?.Count ?? 0;
		}

		// returns text from node
		public string GetTextFromNode(HtmlNode node)
		{
			return node == null ? "null" : node.InnerText.Trim();
		}

		// returns value from attribute
		public string GetAttributeFromNode(HtmlNode node, string attributeName)
		{
			var attributes = node.Attributes.AttributesWithName(attributeName).ToList();
			return attributes[0]?.Value;
		}
	}
}