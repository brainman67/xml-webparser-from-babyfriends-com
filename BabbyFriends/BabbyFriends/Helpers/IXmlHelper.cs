﻿using System.Xml.Linq;

namespace BabbyFriends
{
	interface IXmlHelper
	{
		void CreateXmlDocument();

		void AddNodeOfferToXmlFile(XElement _xElement);

		void AddNodeCategoryToXmlFile(XElement _xElement);

		void SaveXmlFile(XDocument _xDoc);
	}
}