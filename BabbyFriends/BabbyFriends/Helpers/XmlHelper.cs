﻿using BabbyFriends.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace BabbyFriends
{
	public class XmlHelper : IXmlHelper
	{
		private const string XDocName = "BabyFriends.xml";
		private readonly string _rootName = RootAttributes.shop.ToString();
		private readonly string _rootOfferName = RootAttributes.offers.ToString();
		private readonly string _rootCategoryName = RootAttributes.categories.ToString();
		private readonly string _attributeCategoryName = Attributes.category.ToString();
		private readonly string _attributeOfferName = Attributes.offer.ToString();

		public void CreateXmlDocument()
		{
			var xDoc = new XDocument();
			var mainRoot = new XElement(RootAttributes.shop.ToString());
			xDoc.Add(mainRoot);
			SaveXmlFile(xDoc);
			AddBasicNodes(xDoc);

			var rootCategory = new XElement(RootAttributes.categories.ToString(),
					new XElement(Attributes.category.ToString(), "firstCategory",
						new XAttribute(Attributes.id.ToString(), Attributes.id)));

			var rootOffer = new XElement(RootAttributes.offers.ToString(),
				new XElement(Attributes.offer.ToString(), "firstOffer",
					new XAttribute(Attributes.id.ToString(), Attributes.id)));

			var xdoc = XDocument.Load(XDocName);
			var rootShop = xdoc.Element(_rootName);
			rootShop?.Add(rootCategory);
			rootShop?.Add(rootOffer);

			SaveXmlFile(xdoc);
		}

		public void AddNodeOfferToXmlFile(XElement Offer)
		{
			var xdoc = XDocument.Load(XDocName);
			xdoc.Element(_rootName)
				?.Element(_rootOfferName)
				?.Element(_attributeOfferName)
				?.AddBeforeSelf(Offer);
			SaveXmlFile(xdoc);
		}

		public void AddNodesOffersToXmlFile(List<XElement> offerList)
		{
			var xdoc = XDocument.Load(XDocName);
			foreach (var offer in offerList)
			{
				xdoc.Element(_rootName)
					?.Element(_rootOfferName)
					?.Element(_attributeOfferName)
					?.AddBeforeSelf(offer);
			}
			RemoveNodeToIdInXmlFile(xdoc, Attributes.id.ToString());
			SaveXmlFile(xdoc);
		}

		public void AddNodeCategoryToXmlFile(XElement category)
		{
			var xdoc = XDocument.Load(XDocName);
			xdoc.Element(_rootName)
				?.Element(_rootCategoryName)
				?.Element(_attributeCategoryName)
				?.AddBeforeSelf(category);
			SaveXmlFile(xdoc);
		}

		public void AddNodesCategoriesToXmlFile(List<XElement> categoriesList)
		{
			var xdoc = XDocument.Load(XDocName);
			categoriesList.Reverse();
			foreach (var category in categoriesList)
			{
				xdoc.Element(_rootName)
					?.Element(_rootCategoryName)
					?.Element(_attributeCategoryName)
					?.AddBeforeSelf(category);
			}
			RemoveNodeToIdInXmlFile(xdoc, Attributes.id.ToString());
			SaveXmlFile(xdoc);
		}

		public void SaveXmlFile(XDocument xDoc)
		{
			xDoc.Save(XDocName);
		}

		//for cleaning default node
		private void RemoveNodeToIdInXmlFile(XContainer xDoc, string id)
		{
			xDoc.Elements(_rootName).Elements(_rootCategoryName).Elements(_attributeCategoryName)
				.Where(c => c.Attribute("id")?.Value == id).Remove();
		}

		//HARDCODE
		private void AddBasicNodes(XDocument xDoc)
		{
			var nameNode = new XElement("name", "Kid friends");
			var companyNode = new XElement("company", "Kid friends");
			var urlNode = new XElement("url", "https://baby-friends.com/");
			var currencyNode = new XElement("currencies",
				new XElement("currency", new XAttribute("rate", "1"), new XAttribute("id", "UAH")));

			var rootShop = xDoc.Element(_rootName);
			rootShop?.Add(nameNode);
			rootShop?.Add(companyNode);
			rootShop?.Add(urlNode);
			rootShop?.Add(currencyNode);
			SaveXmlFile(xDoc);
		}
	}
}