﻿using System;
using BabbyFriends.Offers;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace BabbyFriends
{
	public class MainBaby : BrawserHelper
	{
		#region Locators
		private const string RootCategoriesXpath = ".//li[@class='b-sub-nav-list__item']";
		private const string OfferXpath = ".//li[@data-qaid='product-block']";
		private const string PageCountXpath = ".//a[@class = ' b-pager__link ']";
		private const string ProductGalleryXpath = ".//ul[@class = 'b-product-gallery']";
		private const string PriceXpath = ".//span[@data-qaid='product_price']";
		private const string NameXpath = ".//span[@data-qaid='product_name']";
		private const string OfferCodeXpath = ".//span[@data-qaid='product_code']";
		private const string DescriptionXpath = ".//*[@data-qaid='product_description']";
		private const string MiniPhotosXpath = ".//a[@class='b-extra-photos__item']";
		private const string PhotoXpath = ".//a[@class='js-product-gallery-overlay b-product-view__image-link']";
		private const string prodactCellXpath = "/td[@class = 'b-product-info__cell']";
		#endregion

		private readonly PageParsingHelper _pageParsingHelper = new PageParsingHelper();
		private readonly StringHelper _stringHelper = new StringHelper();
		private readonly Category _mainCategory = new Category();
		private readonly Offer _currentOffer = new Offer();
		private readonly HtmlDocument htmlDocument = new HtmlDocument();
		private readonly HtmlDocument htmlDocumentForOffer = new HtmlDocument();
		private readonly Dictionary<int, string> _endpointsOffers = new Dictionary<int, string>();
		private static int _increment = 0;

		public List<XElement> GetAllCategories()
		{
			var categoriesList = new List<XElement>();
			var mainPage = GetMainPage();
			htmlDocument.LoadHtml(mainPage);

			var rootCategoriesCount = _pageParsingHelper.GetCountOfNodes(htmlDocument, RootCategoriesXpath);
			for (var i = 1; i <= rootCategoriesCount; i++)
			{
				var singleNode = _pageParsingHelper.GetNodeByXpath(htmlDocument, RootCategoriesXpath + "[" + i + "]/a");
				categoriesList.Add(GetRootCategory(singleNode, out var id));

				var parentCategoriesCount = _pageParsingHelper.GetCountOfNodes(htmlDocument, RootCategoriesXpath + "[" + i + "]/ul/li");
				for (var j = 1; j <= parentCategoriesCount; j++)
				{
					var singleParentNode = _pageParsingHelper.GetNodeByXpath(htmlDocument, RootCategoriesXpath + "[" + i + "]/ul/li[" + j + "]/a");
					categoriesList.Add(GetParentCategory(singleParentNode, id));
				}
			}
			foreach (var variable in _endpointsOffers)
			{
				Console.WriteLine(variable.Key + " :" + variable.Value);
			}
			return categoriesList;
		}

		private XElement GetRootCategory(HtmlNode htmlNode, out string id)
		{
			id = _stringHelper.GetIdFromHref(_pageParsingHelper.GetAttributeFromNode(htmlNode, "href"));
			return _mainCategory.CreateCategory(new CategoryParams()
			{
				Id = id,
				NameCategory = _pageParsingHelper.GetTextFromNode(htmlNode)
			});
		}

		private XElement GetParentCategory(HtmlNode htmlNode, string parentId)
		{
			_endpointsOffers.Add(_increment++, _pageParsingHelper.GetAttributeFromNode(htmlNode, "href"));
			return _mainCategory.CreateParentCategory(new CategoryParams()
			{
				Id = _stringHelper.GetIdFromHref(_pageParsingHelper.GetAttributeFromNode(htmlNode, "href")),
				ParentCategoryId = parentId,
				NameCategory = _pageParsingHelper.GetTextFromNode(htmlNode)
			});
		}

		public List<XElement> GetAllOffers(int numberCategory)
		{
			var offersList = new List<XElement>();

			var ctaegoryEndpoints = _endpointsOffers.FirstOrDefault(x => x.Key == numberCategory).Value;

			var mainPage = GetPage(ctaegoryEndpoints);
			htmlDocument.LoadHtml(mainPage);

			var pagerNode = _pageParsingHelper.GetAllNodesByXpath(htmlDocument, PageCountXpath)?.Last();
			var pageCount = pagerNode == null ? 1 : int.Parse(_pageParsingHelper.GetTextFromNode(pagerNode));

			for (var j = 1; j <= pageCount; j++)
			{
				var offerBlockPage = GetPage(ctaegoryEndpoints + $"/page_{j}");
				htmlDocument.LoadHtml(offerBlockPage);

				var offerCountForCurrentPage = _pageParsingHelper.GetCountOfNodes(htmlDocument, OfferXpath);

				for (var i = 1; i <= offerCountForCurrentPage; i++)
				{
					var singleNode = _pageParsingHelper.GetNodeByXpath(htmlDocument, ProductGalleryXpath + $"/li[{i}]/a[4]");
					var offerEndpoint = _pageParsingHelper.GetAttributeFromNode(singleNode, "href");

					var offerPage = GetPage(offerEndpoint);
					htmlDocumentForOffer.LoadHtml(offerPage);

					var listPicture = new List<string> {_pageParsingHelper.GetAttributeFromNode(
							_pageParsingHelper.GetNodeByXpath(htmlDocumentForOffer, PhotoXpath), "href")};

					var miniPhotoCount = _pageParsingHelper.GetCountOfNodes(htmlDocumentForOffer, MiniPhotosXpath);
					if (miniPhotoCount != 0)
					{
						var colectionPhotos = _pageParsingHelper.GetAllNodesByXpath(htmlDocumentForOffer, MiniPhotosXpath);
						listPicture.AddRange(colectionPhotos.Select(photo => _pageParsingHelper.GetAttributeFromNode(photo, "href")));
					}

					var dictionaryParam = new Dictionary<string, string>();
					var countParams = 0;
					for (var k = 2; k < 15; k++)
					{
						var key = _pageParsingHelper.GetTextFromNode(_pageParsingHelper.GetNodeByXpath(
							htmlDocumentForOffer,
							$".//tr[{k}]/" + prodactCellXpath + "[1]")).Replace("&nbsp;", string.Empty);

						if (key == "null")
						{
							countParams++;
							if (countParams == 2)
							{
								break;
							}
							continue;
						}

						var value = _pageParsingHelper.GetTextFromNode(_pageParsingHelper.GetNodeByXpath(
							htmlDocumentForOffer,
							$".//tr[{k}]/" + prodactCellXpath + "[2]")).Replace("&nbsp;", string.Empty);

						dictionaryParam.Add(key, value);
					}

					offersList.Add(_currentOffer.CreateOffer(new OfferParams()
					{
						Url = offerEndpoint,
						Price = _pageParsingHelper.GetTextFromNode(_pageParsingHelper.GetNodeByXpath(htmlDocumentForOffer, PriceXpath)),
						CurrencyId = "UAH",
						CategoryId = _stringHelper.GetIdFromHref(ctaegoryEndpoints),
						Picture = listPicture,
						Name = _pageParsingHelper.GetTextFromNode(_pageParsingHelper.GetNodeByXpath(htmlDocumentForOffer, NameXpath)) +
							   $"({_pageParsingHelper.GetTextFromNode(_pageParsingHelper.GetNodeByXpath(htmlDocumentForOffer, OfferCodeXpath))})",
						Vendor = "additional",
						Description = _pageParsingHelper.GetTextFromNode(_pageParsingHelper.GetNodeByXpath(htmlDocumentForOffer, DescriptionXpath)),
						StockQuantity = "additional",
						OfferId = _stringHelper.GetIdFromHref(offerEndpoint),
						Available = "true",
						Param = dictionaryParam
					}));
					Console.WriteLine(offersList.Count + " - " + DateTime.Now);
				}
			}
			return offersList;
		}
		public List<XElement> GetAllOffers()
		{
			var offersList = new List<XElement>();
			foreach (var ctaegoryEndpoints in _endpointsOffers)
			{
				var mainPage = GetPage(ctaegoryEndpoints.Value);
				htmlDocument.LoadHtml(mainPage);

				var pagerNode = _pageParsingHelper.GetAllNodesByXpath(htmlDocument, PageCountXpath)?.Last();
				var pageCount = pagerNode == null ? 1 : int.Parse(_pageParsingHelper.GetTextFromNode(pagerNode));

				for (var j = 1; j <= pageCount; j++)
				{
					var offerBlockPage = GetPage(ctaegoryEndpoints.Value + $"/page_{j}");
					htmlDocument.LoadHtml(offerBlockPage);

					var offerCountForCurrentPage = _pageParsingHelper.GetCountOfNodes(htmlDocument, OfferXpath);

					for (var i = 1; i <= offerCountForCurrentPage; i++)
					{
						var singleNode =
							_pageParsingHelper.GetNodeByXpath(htmlDocument, ProductGalleryXpath + $"/li[{i}]/a[4]");
						var offerEndpoint = _pageParsingHelper.GetAttributeFromNode(singleNode, "href");

						var offerPage = GetPage(offerEndpoint);
						htmlDocumentForOffer.LoadHtml(offerPage);

						var listPicture = new List<string>
						{
							_pageParsingHelper.GetAttributeFromNode(
								_pageParsingHelper.GetNodeByXpath(htmlDocumentForOffer, PhotoXpath), "href")
						};

						var miniPhotoCount = _pageParsingHelper.GetCountOfNodes(htmlDocumentForOffer, MiniPhotosXpath);
						if (miniPhotoCount != 0)
						{
							var colectionPhotos =
								_pageParsingHelper.GetAllNodesByXpath(htmlDocumentForOffer, MiniPhotosXpath);
							listPicture.AddRange(colectionPhotos.Select(photo =>
								_pageParsingHelper.GetAttributeFromNode(photo, "href")));
						}

						var dictionaryParam = new Dictionary<string, string>();
						var countParams = 0;
						for (var k = 2; k < 15; k++)
						{
							var key = _pageParsingHelper.GetTextFromNode(_pageParsingHelper.GetNodeByXpath(
								htmlDocumentForOffer,
								$".//tr[{k}]/" + prodactCellXpath + "[1]")).Replace("&nbsp;", string.Empty);

							if (key == "null")
							{
								countParams++;
								if (countParams == 2)
								{
									break;
								}

								continue;
							}

							var value = _pageParsingHelper.GetTextFromNode(_pageParsingHelper.GetNodeByXpath(
								htmlDocumentForOffer,
								$".//tr[{k}]/" + prodactCellXpath + "[2]")).Replace("&nbsp;", string.Empty);

							dictionaryParam.Add(key, value);
						}

						offersList.Add(_currentOffer.CreateOffer(new OfferParams()
						{
							Url = offerEndpoint,
							Price = _pageParsingHelper.GetTextFromNode(
								_pageParsingHelper.GetNodeByXpath(htmlDocumentForOffer, PriceXpath)),
							CurrencyId = "UAH",
							CategoryId = _stringHelper.GetIdFromHref(ctaegoryEndpoints.Value),
							Picture = listPicture,
							Name = _pageParsingHelper.GetTextFromNode(
									   _pageParsingHelper.GetNodeByXpath(htmlDocumentForOffer, NameXpath)) +
								   $"({_pageParsingHelper.GetTextFromNode(_pageParsingHelper.GetNodeByXpath(htmlDocumentForOffer, OfferCodeXpath))})",
							Vendor = "additional",
							Description =
								_pageParsingHelper.GetTextFromNode(
									_pageParsingHelper.GetNodeByXpath(htmlDocumentForOffer, DescriptionXpath)),
							StockQuantity = "additional",
							OfferId = _stringHelper.GetIdFromHref(offerEndpoint),
							Available = "true",
							Param = dictionaryParam
						}));
						Console.WriteLine(offersList.Count + " - " + DateTime.Now);
					}
				}
			}
			return offersList;
		}
	}
}