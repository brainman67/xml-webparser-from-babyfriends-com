﻿using System;

namespace BabbyFriends
{
	class Program
	{
		private static readonly XmlHelper XmlHelper = new XmlHelper();
		private static readonly MainBaby MainBaby = new MainBaby();

		private static void Main(string[] args)
		{
			DoWork();
			Console.ReadKey();
		}

		//test
		private static void DoWork()
		{
			XmlHelper.CreateXmlDocument();
			try
			{
				XmlHelper.AddNodesCategoriesToXmlFile(MainBaby.GetAllCategories());
				Console.WriteLine("Please enter number category do you need: ");
				var number = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
				XmlHelper.AddNodesOffersToXmlFile(MainBaby.GetAllOffers(number));
				//XmlHelper.AddNodesOffersToXmlFile(MainBaby.GetAllOffers());
				//Console.WriteLine("Process adding categories success!!!\n$$$$$My Congratulations$$$$$");
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}
		}
	}
}